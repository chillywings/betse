#!/usr/bin/env python3
# --------------------( LICENSE                            )--------------------
# Copyright 2014-2018 by Alexis Pietak & Cecil Curry.
# See "LICENSE" for further details.

'''
Global unit test configuration for BETSE.

`py.test` implicitly imports all functionality defined by this module into all
unit test modules. As this functionality includes all publicly declared
functional fixtures in this `fixture` subpackage, these tests may reference
these fixtures without explicit imports.
'''

# ....................{ IMPORTS ~ fixture                  }....................
