# --------------------( LICENSE                            )--------------------
# Copyright 2014-2018 by Alexis Pietak & Cecil Curry.
# See "LICENSE" for further details.
#
# --------------------( SYNOPSIS                           )--------------------
# List of all mandatory or optional runtime dependencies of this application to
# be installed by the "conda" package manager bundled with both the default
# Anaconda distribution and minimal Miniconda distribution.
#
# This file conforms to the "conda"- rather than "setuptools"- or "pip"-specific
# file format for dependencies. Hence, the existing dependencies listed by the
# top-level "setup.py" script *CANNOT* be reused here by merely specifying ".".
#
# --------------------( EXAMPLES                           )--------------------
# To generate a sample file in this format describing all currently installed
# "conda" packages, run the following command:
#
#     $ conda list -e

# ....................{ DEPENDENCIES ~ runtime             }....................
# All mandatory and optional runtime dependencies of this application for which
# Anaconda packages are published by either the conda-forge channel (ideally)
# *OR* any of the official default channels.
#
# For simplicity, this list is a partial duplicate of the canonical
# conda-specific dependency list for this application specified by the
# YAML-formatted "requirements:/run:" list in the recipe at our official
# conda-forge feedstock: e.g.,
#
#     https://github.com/leycec/betse-feedstock/blob/master/recipe/meta.yaml
#
# Ideally, these two lists should be manually synchronized as much as feasible. 
# The *ONLY* changes should be the following:
#
# * This list should be revised where necessary to reflect changes between the
#   live and most recent stable versions of this application.
# * This list intentionally omits the following dependencies:
#   * The optional "ffmpeg" dependency, which is currently *ONLY* available on
#     all supported platforms via the unstable "conda-forge" channel. (Note that
#     the stable "anaconda" channel currently provides *NO* Windows package for
#     this dependency, despite providing macOS and Linux packages).
#   * The optional "pyside2" dependency, which applies *ONLY* to interactive
#     environments.
#   * The optional interrelated "graphviz", "networkx", and "pydot"
#     dependencies, which currently fail to behave as expected under Windows.
#     Specifically, the following exception suggesting Windows to be unable to
#     find GraphViz in the %PATH% is raised:
#         FileNotFoundError: [WinError 2] "dot.exe" not found in path.
setuptools >=3.3
dill >=0.2.3
#graphviz >=2.38.0
matplotlib >=1.5.0
#networkx >=2.0
numpy >=1.8.2
pillow >=2.3.0
#pydot >=1.0.28
pyyaml >=3.10
scipy >=0.12.0
six >=1.5.2
#ffmpeg
#pyside2

# ....................{ DEPENDENCIES ~ test                }....................
# All mandatory test dependencies of this application, manually converted from
# the "betse.metadata.TESTTIME_MANDATORY" global.
pytest
