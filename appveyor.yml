# --------------------( LICENSE                            )--------------------
# Copyright 2014-2018 by Alexis Pietak & Cecil Curry.
# See "LICENSE" for further details.
#
# --------------------( SYNOPSIS                           )--------------------
# Project-wide Appveyor configuration, integrating the third-party
# Windows-specific free-as-in-beer continuous integration (CI) service exposed
# by Appveyor with this project's "py.test"-driven test suite.
#
# Due to the non-trivial complexity in computational space, computational time,
# and developer time and sanity of installing scientific Python packages
# (particularly packages requiring C extensions) via "pip", this configuration
# leverages the Miniconda distribution instead. Doing so dramatically reduces
# the aforementioned complexity with a minor increase in redundancy, requiring
# the mandatory dependencies already declared by this project's top-level
# "setup.py" script as PyPI-specific packages be redeclared as
# Miniconda-specific packages. While unctuous, the otherwise significant
# reduction in complexity will brook no argument.
#
# --------------------( SEE ALSO                           )--------------------
# * https://www.appveyor.com/docs/installed-software/#python
#   *THIS IS ABSOLUTELY ESSENTIAL,* providing the canonical list of all Python-
#   oriented software unconditionally installed into all CI environments
#   complete with the absolute paths, word sizes, and version specifiers of such
#   software. The configuration below is critically dependent upon this list.
# * http://tjelvarolsson.com/blog/how-to-continuously-test-your-python-code-on-windows-using-appveyor/
#   Relatively recent, reasonably well-written blog post articulating the
#   simplest Appveyor configuration establishing a sane Miniconda testing
#   environment *WITHOUT* the usual obsolete and hence insane edge-case handling
#   typically littering these configurations.
# * https://github.com/conda-forge/staged-recipes/blob/master/appveyor.yml
#   Real-world "appveyor.yml" configuration manually establishing a sane
#   Miniconda testing environment *WITHOUT* leveraging external automation
#   (e.g., the "appveyor/install-miniconda.ps1" script). This configuration
#   arguably remains the de-facto and most popular solution for doing so.
# * https://raw.githubusercontent.com/astropy/astropy/master/appveyor.yml
#   Real-world "appveyor.yml" configuration used by a prominent scientific
#   Python framework and largely considered to be the canonical scientific
#   Appveyor configuration as of this writing. The "appveyor.yml" files of
#   multiple other projects (e.g., SpiceyPy) reference this "appveyor.yml" file,
#   which does indeed appear to be subjectively authoritative.
# * https://github.com/astropy/ci-helpers
#   Utility scripts run by the above configuration to establish a sane Miniconda
#   testing environment, notably the "appveyor/install-miniconda.ps1" script.
#   These scripts are sufficiently general-purpose and well-documented as to be
#   cloned and run by an assortment of third-party Appveyor configuration files
#   unaffiliated with the AstroPy project. Doing so presents numerous concerns,
#   however, including:
#   * Efficiency, as:
#     * Each Appveyor run would repetitively clone this repository.
#     * "install-miniconda.ps1" is technically specific to the AstroPy project
#       and thus contains logic of interest *ONLY* to that project.
#     * "install-miniconda.ps1" predates Appveyor's useful decision to
#       unconditionally install Miniconda into all CI environments and has yet
#       to be refactored accordingly. Because of this, that script expends
#       non-trivial space and time uselessly re-downloading and re-installing
#       Miniconda into the current CI environment.
#   * Maintainability, as that repository is under third-party control.
#   This Appveyor configuration has opted to manually establish a sane Miniconda
#   testing environment without leveraging this repository, preserving both
#   efficiency and maintainability.
# * https://packaging.python.org/appveyor
#   Quasi-official documentation providing the canonical "appveyor.yml"
#   skeleton configuration for use in existing Python projects. Sadly, this
#   skeleton leverages the general-purpose "pip" package manager rather than the
#   Anaconda-specific "conda" package manager and hence is sadly useless.

#FIXME: Auto-generate a pip-compliant binary Windows wheel file for this project
#*AFTER* all successful test runs. For examples, see:
#    https://raw.githubusercontent.com/AndrewAnnex/SpiceyPy/master/appveyor.yml

# ....................{ CONFIGURATION                      }....................
# Disable .NET integration. Python projects are implicitly built during
# installation.
build: false


matrix:
  # Enable the so-called "fast fail strategy," halting the entire test process
  # on the first profile failure.
  fast_finish: true

# ....................{ CONFIGURATION ~ matrix             }....................
platform:
  # Exercise only 64-bit binaries.
  - x64


environment:
  # Dictionary mapping from the name to the value of each global environment
  # variable to unconditionally declare for *ALL* build profiles defined by the
  # "matrix" list below.
  # global:
    # Whitespace-delimited list of the names of all Miniconda channels required
    # by third-party Miniconda packages listed in "CONDA_DEPENDENCIES".
    # CONDA_CHANNELS: ""

    # Whitespace-delimited list of the names of all PyPI-packaged packages to be
    # installed with "pip" required by this project and *NOT* installable with
    # Miniconda. This is a fallback that should be used only where needed.
    # PIP_PACKAGE_NAMES: ""

  # List of all build profiles, each of which is a dictionary mapping from the
  # name to the value of each global environment variable to be conditionally
  # declared for this profile.
  #
  # This matrix exercises all supported major 64-bit Python 3.x versions,
  # ignoring changes between minor versions, all Python 2.x versions, *AND* all
  # 32-bit Python versions. While this project technically supports the same
  # 32-bit Python 3.x versions, 32-bit architectures are explicitly unsupported
  # (e.g., due to the 4GB memory barrier) and hence ignorable. See also:
  #
  # * The "betse.metadata.PYTHON_VERSION_MIN" string global, defining the
  #   minimum version of Python 3.x supported by this project. All stable
  #   versions of Python 3.x greater than or equal to this minimum version
  #   *MUST* be explicitly listed below.
  # * The "betse.metadeps.RUNTIME_MANDATORY" tuple global, defining the minimum
  #   version of Numpy supported by this project.
  matrix:
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    # CAUTION: Although both this project and Miniconda explicitly support Python 3.4,
    # Miniconda no longer appears capable of installing SciPy when running Python 3.4.
    # Attempting to do so reliably induces the following exception:
    #
    #    _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
    #    betse_test\fixture\simconf\simconfclser.py:173: in __init__
    #        from betse_test.fixture.simconf.simconfwrapper import (
    #    betse_test\fixture\simconf\simconfwrapper.py:35: in <module>
    #        from betse.science.visual.anim.animpipe import AnimCellsPipe
    #    betse\science\visual\anim\animpipe.py:20: in <module>
    #        from betse.science.visual.anim.anim import (
    #    betse\science\visual\anim\anim.py:37: in <module>
    #        from betse.science.visual.plot.plotutil import cell_mosaic, cell_mesh
    #    betse\science\visual\plot\plotutil.py:14: in <module>
    #        from scipy import interpolate
    #    C:\Miniconda3-x64\envs\conda-env-3.4\lib\site-packages\scipy\interpolate\__init__.py:176: in <module>
    #        from .interpolate import *
    #    C:\Miniconda3-x64\envs\conda-env-3.4\lib\site-packages\scipy\interpolate\interpolate.py:21: in <module>
    #        import scipy.special as spec
    #    _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
    #        """
    #        
    #        from __future__ import division, print_function, absolute_import
    #        
    #        from .sf_error import SpecialFunctionWarning, SpecialFunctionError
    #        
    #    >   from ._ufuncs import *
    #    E   ImportError: DLL load failed: The specified module could not be found.
    #
    # Googling strongly suggests this exception to be raised by improperly packaged SciPy
    # installations under Windows. Consequently, this project's support for Python 3.4 is
    # no longer exercised (i.e., tested) under Windows.
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    # Dot-delimited Python version required by this profile.
    - PYTHON_VERSION: 3.6
      # Absolute path of the Miniconda version specific to this Python version.
      MINICONDA_DIRNAME: C:\Miniconda36-x64

    # Dot-delimited Python version required by this profile.
    - PYTHON_VERSION: 3.5
      # Absolute path of the Miniconda version specific to this Python version.
      MINICONDA_DIRNAME: C:\Miniconda35-x64

# ....................{ COMMANDS                           }....................
# List of all external commands to be run for each profile *BEFORE* those listed
# within "install" below.
init:
  # For debuggability, visually prefix each profile by (in order):
  #
  # * The absolute path of the active Python interpreter.
  # * The version of this interpreter.
  # * The word size of this interpreter.
  # * The absolute path of Miniconda.
  - ECHO %PYTHON_VERSION% %MINICONDA_DIRNAME%


# List of all external commands to be run for each profile *BEFORE* those listed
# within "test_script" below.
install:
  # Prepend the current %PATH% by the absolute paths of all Miniconda-managed
  # directories containing the active Python interpreter and standard Python
  # scripts (e.g., "easy_install") installed with this interpreter, ensuring
  # that the Miniconda-specific rather than system-wide version of Python is run
  # (e.g., "C:\Miniconda3-x64" rather than "C:\Python34-x64"). This *MUST* be
  # done prior to running the "conda" command.
  - cmd: set "PATH=%MINICONDA_DIRNAME%;%MINICONDA_DIRNAME%\\Scripts;%PATH%"

  # Configure "conda" to run in headless mode. Dismantled, this is:
  #
  # * "always_yes true", automatically pass the "--yes" option to *ALL*
  #   "conda" commands run below. This is a superficial convenience reducing the
  #   likelihood of developer oversight and hence saving essential sanity.
  #
  # Note that the "auto_update_conda" option is unsupported by the older
  # versions of "conda" installed with Appveyor and hence omitted here.
  - cmd: conda config --set always_yes true

  # Update Miniconda to its most recent stable release.
  - cmd: conda update --quiet conda

  # For debuggability, print metadata identifying this Miniconda release.
  - cmd: conda info --all

  #FIXME: This environment should probably be cached. Consider researching
  #Appveyor-specific conda caching.

  # Create an empty Anaconda environment specific to this Python version.
  - cmd: conda create --quiet --name conda-env-%PYTHON_VERSION% python=%PYTHON_VERSION%

  # Activate this environment (i.e., prepend the current %PATH% by this
  # environment's top-level directory).
  - cmd: activate conda-env-%PYTHON_VERSION%

  # Add the official "anaconda" channel as the highest-priority channel.
  # See similar logic in ".gitlab-ci.yml" for details.
  - cmd: conda config --add channels anaconda
    
  # Install all mandatory and optional dependencies of this application into
  # this environment from the "conda-forge" channel.
  - cmd: conda install --quiet --file requirements-conda.txt

  # Install this project into this environment in the most efficient means
  # possible (i.e., without copying this project into this environment).
  - cmd: python setup.py develop


# List of all external commands to be run for each CI pipeline *AFTER* those
# listed within "install" above.
test_script:
  #FIXME: See ".gitlab-ci.yml" for why this is currently disabled.
  # For debuggability, print metadata identifying this BETSE release. See
  # similar logic in ".gitlab-ci.yml" for details.
  #- cmd: betse --matplotlib-backend=agg info

  # Run the following multiline Powershell script.
  - ps: |
      # Run the entire "py.test"-based test suite under the following options:
      #
      # * "--maxfail=3", halting testing on the third failure. For discussion, see
      #   the "betse_setup.test" submodule.
      py.test --maxfail=3

      #FIXME: To assist in debugging this, also print a non-fatal warning with the
      #actual exit status reported above.

      # If the exit status reported by "py.test" is non-zero but nonsensical,
      # coerce this status to zero. For unknown reasons, some hellish combination
      # of Appveyor, Python, "py.test", and our test suite causes successful runs
      # to unpredictably report nonsensical non-zero exit status. To preserve
      # developer sanity, these false negatives *MUST* be explicitly ignored.
      # Happily, the exit status reported by these false negatives all reside in
      # the same range of extremely negative integers.
      #
      # Note that Powershell sets the $LastExitCode integer global *ONLY* on
      # running external commands. If statements do *NOT* trigger this behaviour.
      # Hence, the current value of this global is preserved if this if statement
      # evaluates to False.
      if ($LastExitCode -le -1000000000) { $host.SetShouldExit(0) }
